# Penny Pew's Excellent Web Based Distraction-Free Plain Text Editor

* Open menu using ² or `
* Highlight content by putting it between #
* Scroll through highlighted content with ALT+ARROWS (UP or DOWN)
* Download a .txt copy of your text using ALT+S
* Press F11 (depending on browser setting) to enter full screen